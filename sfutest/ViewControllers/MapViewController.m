//
//  MapViewController.m
//  sfutest
//
//  Created by Teymur Akhundov on 2/26/15.
//  Copyright (c) 2015 Teymur Akhundov. All rights reserved.
//ß

#import "MapViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface MapViewController ()

@end

@implementation MapViewController {
    GMSMapView *mapView_;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadView {
    //SFU: Latitude: 49.279621 | Longitude: -122.920307
    
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate -33.86,151.20 at zoom level 6.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:49.279621
                                                            longitude:-122.920307
                                                                 zoom:14];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.myLocationEnabled = YES;
    self.view = mapView_;
    
    //[self addMarker:49.279621 longitude:-122.920307 title:@"SFU" snippet:@"Simon Fraser University"];
    
    
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(49.279621, -122.920307);
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.icon = [UIImage imageNamed:@"bus48"];
    marker.map = mapView_;

    
    
    
    GMSMutablePath *path = [GMSMutablePath path];
    [path addLatitude:49.28230813048122 longitude:-122.93379306793213]; // top left
    [path addLatitude:49.281748225852034 longitude:-122.90117740631104]; // top right
    [path addLatitude:49.27077281171215 longitude:-122.903151512146]; // bot right
    [path addLatitude:49.27628882255954 longitude:-122.93608903884888]; // bot left
    [path addLatitude:49.28230813048122 longitude:-122.93379306793213]; // top left
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeColor = [UIColor blueColor];
    polyline.strokeWidth = 1.f;
    polyline.map = mapView_;
    
    
}

- (void)addMarker:(double)lat longitude:(double)lon title:(NSString *)title snippet:(NSString *)snippet
{
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(lat, lon);
    marker.title = title;
    marker.snippet = snippet;
    marker.map = mapView_;
    
    
    
    
    
    
}


@end
